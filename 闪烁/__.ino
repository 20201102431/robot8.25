#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include <MeMCore.h>


MeRGBLed rgbled_7(7, 7==7?2:4);

void setup(){
    
}

void loop(){
        rgbled_7.setColor(0,60,0,0);
        rgbled_7.show();
        delay(1000);
        rgbled_7.setColor(0,0,0,0);
        rgbled_7.show();
        delay(1000);
}
